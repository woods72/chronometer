package com.company.Chrono;

/**
 * Created by root on 09.06.17.
 */
public class Chronometer extends Thread {

    public static volatile Integer sec = 0;
    private int interval;

    public Chronometer() {
        this.interval = 1;
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("Every 1 sec: " + sec);
                Thread.sleep(1000);
                sec += 1;
                synchronized (Main.monitor) {
                    Main.monitor.notifyAll();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
