package com.company.Chrono;

/**
 * Created by root on 09.06.17.
 */
public class Messenger extends Thread {
    private int interval;

    public Messenger(int interval) {
        this.interval = interval;
    }

    @Override
    public void run() {
        while (true) {
            try {
                synchronized (Main.monitor) {
                    Main.monitor.wait();
                }
                if (Chronometer.sec % interval == 0 && Chronometer.sec != 0) {
                    System.out.println(interval + " sec: " + Chronometer.sec);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
