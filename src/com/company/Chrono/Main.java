package com.company.Chrono;

public class Main {

    public static volatile Object monitor = new Object();

    public static void main(String[] args) {
	// write your code here

        Chronometer ch = new Chronometer();
        Messenger msg5 = new Messenger(5);
        Messenger msg7 = new Messenger(7);

        ch.start();
        msg5.start();
        msg7.start();
    }
}
